package com.parallelstream;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public class Shop {
    String shopName;

    public Shop(String name) {
        shopName = name;
    }

    public String getShopName(){
        return shopName;
    }

    public double getPrice(String product) {
        return calculatePrice(product);
    }

    public Future<Double> getPriceAsync(String product) {
        CompletableFuture<Double> futurePrice = new CompletableFuture<Double>();
        new Thread(() -> {
            try {
                double price = calculatePrice(product);
                futurePrice.complete(price);
            } catch (Exception e) {
                futurePrice.completeExceptionally(e);
            }
        }).start();
        return futurePrice;
    }

    public Future<Double> getPriceAsyncV2(String product) {
        return CompletableFuture.supplyAsync(() -> calculatePrice(product));
    }

    public double calculatePrice(String product) {
//        Shop.delay();
        Shop.randomDelay();
        Random random = new Random();
        return random.nextDouble() * product.charAt(0) + product.charAt(1);
    }

    public static void delay() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static void randomDelay (){
        try {
            Random random = new Random();
            long duration = 500 + random.nextInt(2000);
            Thread.sleep(duration);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public String getPriceWithDiscount(String product) {
        double price = calculatePrice(product);
        Random random = new Random();
        Discount.Code discountCode = Discount.Code.values()[
                random.nextInt(Discount.Code.values().length)];
        return String.format("%s:%f:%s", shopName, price, discountCode);
    }
}
