package com.parallelstream;

import static com.parallelstream.Shop.delay;

public class Discount {
    public enum Code {
        NONE(0),
        SILVER(5),
        GOLD(10),
        PLATINUM(15),
        DIAMOND(20);

        private final int percentage;

        Code(int percentage) {
            this.percentage = percentage;
        }
    }

    public static String applyDiscount(Quote quote) {
        return quote.getShopName() + " price is " + Discount.applyDiscount(quote.getPrice(), quote.getDiscountCode());
    }

    public static String applyDiscount(double price, Code discountCode) {
        delay();
        return String.format("%f", price * (100 - discountCode.percentage) / 100);
    }
}
