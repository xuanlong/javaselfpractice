package com.parallelstream;


import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainClass {
    static List<Shop> shops = Arrays.asList(new Shop("BestPrice"),
            new Shop("LetsSaveBig"),
            new Shop("MyFavoriteShop"),
            new Shop("BuyItAll"),
            new Shop("The fifth Shop"),
            new Shop("The sixth Shop"),
            new Shop("The seventh Shop"),
            new Shop("The eighth Shop"),
            new Shop("The ninth Shop"),
            new Shop("The tenth Shop"),
            new Shop("The eleventh Shop"),
            new Shop("The twelfth Shop"));

    public static void main(String[] args) {
//        executeFindPricesInAsynchronous();
//        executeFindPricesInParallelStream();
//        executeFindPricesInSequentialFuture();
//        executeFindPricesInFuture();
//        executeFindPricesInFutureWithExecutor();

//        executeFindPricesWithDiscountAsynchronous();
//        executeFindPricesWithDiscountInExecutor();
        executeFindPricesWithDiscountInEarly();
    }

    private static void executeFindPricesInAsynchronous() {
        System.out.println("Run in synchronous");
        long start = System.nanoTime();
        System.out.println(findPrices("myPhone27s"));
        long duration = (System.nanoTime() - start) / 1000000;
        System.out.println("Done in: " + duration + "msecs");
        System.out.println();
    }

    private static void executeFindPricesInParallelStream() {
        long start;
        long duration;
        System.out.println("Run in parallel streams");
        start = System.nanoTime();
        System.out.println(findPricesParallel("myPhone27s"));
        duration = (System.nanoTime() - start) / 1000000;
        System.out.println("Done in: " + duration + "msecs");
        System.out.println();
    }

    private static void executeFindPricesInSequentialFuture() {
        long start;
        long duration;
        System.out.println("Run in sequential future");
        start = System.nanoTime();
        System.out.println(findPricesFutureSequential("myPhone27s"));
        duration = (System.nanoTime() - start) / 1000000;
        System.out.println("Done in: " + duration + "msecs");
        System.out.println();
    }

    private static void executeFindPricesInFuture() {
        long start;
        long duration;
        System.out.println("Run in future");
        start = System.nanoTime();
        System.out.println(findPricesFuture("myPhone27s"));
        duration = (System.nanoTime() - start) / 1000000;
        System.out.println("Done in: " + duration + "msecs");
        System.out.println();
    }

    private static void executeFindPricesInFutureWithExecutor() {
        long start;
        long duration;
        System.out.println("Run in future with executor");
        start = System.nanoTime();
        System.out.println(findPricesFutureWithExecutor("myPhone27s"));
        duration = (System.nanoTime() - start) / 1000000;
        System.out.println("Done in: " + duration + "msecs");
        System.out.println();
    }

    private static void executeFindPricesWithDiscountAsynchronous() {
        long start;
        long duration;
        System.out.println("Run in future with executor");
        start = System.nanoTime();
        System.out.println(findPricesWithDiscountInSynchronous("myPhone27s"));
        duration = (System.nanoTime() - start) / 1000000;
        System.out.println("Done in: " + duration + "msecs");
        System.out.println();
    }

    private static void executeFindPricesWithDiscountInExecutor() {
        long start;
        long duration;
        System.out.println("Run in future with executor");
        start = System.nanoTime();
        System.out.println(findPricesWithDiscountInExecutor("myPhone27s"));
        duration = (System.nanoTime() - start) / 1000000;
        System.out.println("Done in: " + duration + "msecs");
        System.out.println();
    }

    private static void executeFindPricesWithDiscountInEarly() {
        System.out.println("Run in future with executor");
        long start = System.nanoTime();
        CompletableFuture[] futures = findPricesWithDiscountInEarly("myPhone27s")
                .map(future -> future.thenAccept(s -> {
                    System.out.println(s);
                    long duration = (System.nanoTime() - start) / 1000000;
                    System.out.println("Done in: " + duration + "msecs");
                    System.out.println();
                }))
                .toArray(size -> new CompletableFuture[size]);
        CompletableFuture.allOf(futures).join();
    }

    public static List<String> findPrices(String product) {
        return shops.stream()
                .map(shop -> {
                    return String.format("%s price is %.2f",
                            shop.getShopName(), shop.getPrice(product));
                })
                .collect(Collectors.toList());
    }

    public static List<String> findPricesParallel(String product) {
        return shops.stream().parallel()
                .map(shop -> {
                    return String.format("%s price is %.2f", shop.getShopName(), shop.getPrice(product));
                })
                .collect(Collectors.toList());
    }

    public static List<String> findPricesFuture(String product) {
        List<CompletableFuture<String>> futurePrices = shops.stream()
                .map(shop -> {
                    return CompletableFuture.supplyAsync(() -> {
                        return String.format("%s price is %.2f", shop.getShopName(), shop.getPrice(product));
                    });
                })
                .collect(Collectors.toList());

        return futurePrices.stream()
                .map(CompletableFuture::join)
                .collect(Collectors.toList());
    }

    public static List<String> findPricesFutureSequential(String product) {
        return shops.stream()
                .map(shop -> {
                    return CompletableFuture.supplyAsync(() -> {
                        return String.format("%s price is %.2f", shop.getShopName(), shop.getPrice(product));
                    });
                })
                .map(futureString -> futureString.join())
                .collect(Collectors.toList());
//                .collect(Collectors.toList())
//                .stream()
//                .map(futureString -> futureString.join())
//                .collect(Collectors.toList());
    }

    public static List<String> findPricesFutureWithExecutor(String product) {
        Executor executor = Executors.newFixedThreadPool(Math.min(shops.size(), 100),
                new ThreadFactory() {
                    @Override
                    public Thread newThread(Runnable r) {
                        Thread t = new Thread(r);
                        t.setDaemon(true);
                        return t;
                    }
                });

        List<CompletableFuture<String>> futurePrices = shops.stream()
                .map(shop -> {
                    return CompletableFuture.supplyAsync(() -> {
                        return String.format("%s price is %.2f", shop.getShopName(), shop.getPrice(product));
                    }, executor);
                })
                .collect(Collectors.toList());

        return futurePrices.stream()
                .map(CompletableFuture::join)
                .collect(Collectors.toList());
    }

    public static List<String> findPricesWithDiscountInSynchronous(String product) {
        return shops.stream()
                .map(shop -> shop.getPriceWithDiscount(product))
                .map(Quote::parse)
                .map(Discount::applyDiscount)
                .collect(Collectors.toList());
    }

    public static List<String> findPricesWithDiscountInExecutor(String product) {
        Executor executor = Executors.newFixedThreadPool(Math.min(shops.size(), 100),
                new ThreadFactory() {
                    @Override
                    public Thread newThread(Runnable r) {
                        Thread t = new Thread(r);
                        t.setDaemon(true);
                        return t;
                    }
                });

        return shops.stream()
                .map(shop -> {
                    return CompletableFuture.supplyAsync(() -> {
                        return shop.getPriceWithDiscount(product);
                    }, executor);
                })
                .map(future -> future.thenApply(Quote::parse))
                .map(future -> future.thenCompose(quote -> {
                    return CompletableFuture.supplyAsync(() -> {
                        return Discount.applyDiscount(quote);
                    }, executor);
                }))
                .collect(Collectors.toList())
                .stream()
                .map(CompletableFuture::join)
                .collect(Collectors.toList());
    }

    public static Stream<CompletableFuture> findPricesWithDiscountInEarly(String product) {
        Executor executor = Executors.newFixedThreadPool(Math.min(shops.size(), 100), new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r);
                t.setDaemon(true);
                return t;
            }
        });

        return shops.stream()
                .map(shop -> {
                    return CompletableFuture.supplyAsync(() -> {
                        return shop.getPriceWithDiscount(product);
                    }, executor);
                })
                .map(future -> future.thenApply(Quote::parse))
                .map(future -> future.thenCompose(quote -> {
                    return CompletableFuture.supplyAsync(() -> {
                        return Discount.applyDiscount(quote);
                    }, executor);
                }));
    }
}
