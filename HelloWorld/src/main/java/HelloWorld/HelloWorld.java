
package HelloWorld;
import org.jetbrains.annotations.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

interface  TestLambda
{
    void run();
}

class MethodReference
{
    static void callFromStatic() {
        System.out.println("call from static method");
    }

    void callFromInstance() {
        System.out.println("call from instance of class");
    }
}
//import com.sun.istack.internal.NotNull;
public class HelloWorld {
    public static void main(String[] args) {
//        String[] strs = new String[]{
//                "abc",
//                "xyz",
//                "vwt"
//        };
//
//        Arrays.stream(strs).forEach(s -> System.out.println(s));

//        List<String> strs = new ArrayList<>();
//        strs.add("vwt");
//        strs.add("abc");
//        strs.add("xyz");
//        Comparator<String> comparator = (s1, s2) -> {
//            return s1.compareTo(s2);
//        } ;
//        strs.stream().forEach(s -> System.out.println(s));
////        strs.sort((o1, o2) -> o1.compareTo(o2));
//        strs.sort(comparator.reversed());
//        strs.stream().forEach(s -> System.out.println(s));
//        callString(null);
//        callString("abc");

//        Runnable r1 = MethodReference::callFromInstance;

        TestLambda lam = ()  -> {
            System.out.println("Hello with running inside Lambda");
        };
        lam.run();
    }

    /**
     *
     * @param param
     */
    static void callString (@NotNull String param)
    {
        System.out.println(param);
    }
}
