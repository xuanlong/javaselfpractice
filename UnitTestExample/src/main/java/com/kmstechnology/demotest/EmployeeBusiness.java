package com.kmstechnology.demotest;

public class EmployeeBusiness {
    public double calculateYearlySalary(EmployeeDetails details) {
        double yearlySalary = 0;
        yearlySalary = details.getMonthlySalary() * 12;
        return yearlySalary;
    }

    public double calculateAppraisal(EmployeeDetails details) {
        double appraisal = 0;
        if (details.getMonthlySalary() > 10000) {
            appraisal = 1000;
        } else {
            appraisal = 500;
        }
        return appraisal;
    }
}
