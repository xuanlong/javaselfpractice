package com.kmstechnolgy.demomockito;

public class MathApplication {
    private CalculatorService calculatorService;

    public void setCalculatorService(CalculatorService service) {
        this.calculatorService = service;
    }

    public double add(double par1, double par2) {
        return this.calculatorService.add(par1, par2);
    }

    public double subtract(double par1, double par2) {
        return this.calculatorService.subtract(par1, par2);
    }

    public double multiply(double par1, double par2) {
        return this.calculatorService.multiply(par1, par2);
    }

    public double divide(double par1, double par2) {
        return this.calculatorService.divide(par1, par2);
    }
}
