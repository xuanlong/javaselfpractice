package com.kmstechnolgy.demomockito;

public interface CalculatorService {
    double add(double par1, double par2);

    double subtract(double par1, double par2);

    double multiply(double par1, double par2);

    double divide(double par1, double par2);
}
