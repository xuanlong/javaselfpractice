package com.kmstechnology.demotest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runners.JUnit4;

import javax.swing.undo.AbstractUndoableEdit;

import static org.junit.Assert.*;

public class EmployeeBusinessTest {

    @Test
    public void testCalculateYearlySalary() {
        EmployeeDetails details = new EmployeeDetails();
        details.setAge(39);
        details.setMonthlySalary(1000);
        details.setName("John");

        EmployeeBusiness business = new EmployeeBusiness();
        Assert.assertEquals(12000, business.calculateYearlySalary(details), 0.0);
//        assertEquals(business.calculateYearlySalary(details), 12000);
    }

    @Test
    public void testCalculateAppraisal() {
        EmployeeDetails details = new EmployeeDetails();
        details.setAge(39);
        details.setMonthlySalary(1000);
        details.setName("John");

        EmployeeBusiness business = new EmployeeBusiness();
        Assert.assertEquals(500, business.calculateAppraisal(details), 0.0);
    }
}