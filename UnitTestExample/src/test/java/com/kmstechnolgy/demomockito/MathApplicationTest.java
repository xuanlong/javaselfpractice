package com.kmstechnolgy.demomockito;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MathApplicationTest {
    @InjectMocks
    MathApplication mathApplication = new MathApplication();

    @Mock
    CalculatorService calculatorService;

    @Test
    public void testAdd ()
    {
//        when(calculatorService.add(10.0, 20.0)).thenReturn(30.0);
//        when(calculatorService).add(10.0. 20.0).thenR
        assertEquals(mathApplication.add(10.0, 20.0), 30.0, 0.0);
        verify(calculatorService).add(10, 30);
    }
}