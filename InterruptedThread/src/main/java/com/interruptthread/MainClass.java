package com.interruptthread;

public class MainClass {
    public static void main(String[] args) {
        Thread t = new Thread(){
            @Override
            public void run() {
                try {
                    sleep(5000);
                } catch (InterruptedException e) {
                    System.out.println(e.toString());
                }
            }
        };

//        t.interrupt();
        t.start();
        t.interrupt();
    }
}
